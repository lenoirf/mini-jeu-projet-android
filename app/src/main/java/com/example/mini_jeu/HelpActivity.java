package com.example.mini_jeu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class HelpActivity extends AppCompatActivity {
    private Button button_retour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        button_retour = findViewById(R.id.button3);
        button_retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent conIntent = new Intent(view.getContext(), JeuActivity.class);
                startActivityForResult(conIntent, 0);
            }
        });

    }
}
