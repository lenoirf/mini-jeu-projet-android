package com.example.mini_jeu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DifficultyActivity extends AppCompatActivity {
    private Button button_jouer;
    private Spinner spinner_difficulty;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_difficulty);

        tv=findViewById(R.id.textView6);
        spinner_difficulty=findViewById(R.id.spinner);
        String[] arraySpinner = new String[] {
                "1", "2", "3","4","5"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_difficulty.setAdapter(adapter);

        button_jouer = findViewById(R.id.button1);
        button_jouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent conIntent = new Intent(view.getContext(), JeuActivity.class);
                int difficulte=Integer.parseInt(spinner_difficulty.getSelectedItem().toString());
                conIntent.putExtra("niveau_Difficulte", difficulte);
                startActivity(conIntent);
            }
        });
    }
}
