package com.example.mini_jeu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button button_connexion;
    private Button button_inscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_connexion = findViewById(R.id.button1);
        button_connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent conIntent = new Intent(view.getContext(), ConnexionActivity.class);
                startActivityForResult(conIntent, 0);
            }
        });

        button_inscription = findViewById(R.id.button2);
        button_inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent conIntent = new Intent(view.getContext(), InscriptionActivity.class);
                startActivityForResult(conIntent, 0);
            }
        });

    }


}
